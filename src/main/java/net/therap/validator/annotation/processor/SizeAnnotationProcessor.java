package net.therap.validator.annotation.processor;

import net.therap.validator.annotation.Size;
import net.therap.validator.model.ValidationError;

import java.lang.reflect.Field;

/**
 * @author shafin
 * @since 11/8/16
 */
public class SizeAnnotationProcessor extends AnnotationProcessor {

    public SizeAnnotationProcessor() {

    }

    @Override
    public ValidationError processAnnotation(Object value, Field field) {

        Size sizeAnnotation = field.getAnnotation(Size.class);
        if (sizeAnnotation == null) {
            return null;
        }

        int minSize = sizeAnnotation.min();
        int maxSize = sizeAnnotation.max();

        if (field.getType().equals(String.class)) {
            String stringVal = (String) value;
            if (stringVal.length() < minSize | stringVal.length() > maxSize) {
                return formatErrorMessage(field.getName(), field.getType().getSimpleName(),
                        sizeAnnotation.message(), minSize, maxSize);
            }
        } else if (field.getType().equals(int.class)) {
            int integerVal = (Integer) value;
            if (integerVal < minSize || integerVal > maxSize) {
                return formatErrorMessage(field.getName(), field.getType().getSimpleName(),
                        sizeAnnotation.message(), minSize, maxSize);
            }
        }
        return null;
    }

    private ValidationError formatErrorMessage(String fieldName, String typeName, String message, int min, int max) {
        String errorMessage = message
                .replaceAll("\\{min\\}", String.valueOf(min))
                .replaceAll("\\{max\\}", String.valueOf(max));
        return new ValidationError(fieldName, typeName, errorMessage);
    }
}
