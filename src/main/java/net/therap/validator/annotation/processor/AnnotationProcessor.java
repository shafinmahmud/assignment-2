package net.therap.validator.annotation.processor;

import net.therap.validator.annotation.Size;
import net.therap.validator.model.ValidationError;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * @author shafin
 * @since 11/8/16
 */
public class AnnotationProcessor {

    /*
     * This method is to find and instantiate the relevant AnnotationProcessor.
     * Separate subclasses of AnnotationProcessor should be available to implement
     * separate implementation logic of the associate Annotation.
     */
    public static AnnotationProcessor getRelevantProcessor(Annotation annotation) {
        if (annotation.annotationType().equals(Size.class)) {
            return new SizeAnnotationProcessor();
        }
        return null;
    }

    public ValidationError processAnnotation(Object val, Field field) {
        return null;
    }
}
