package net.therap.validator.service;

import net.therap.validator.annotation.processor.AnnotationProcessor;
import net.therap.validator.model.ValidationError;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shafin
 * @since 11/7/16
 */
public class AnnotatedValidator {

    public static List<ValidationError> validate(Object validatingObject) {
        List<ValidationError> errors = new ArrayList<>();
        Class personClass = validatingObject.getClass();
        Field[] fields = personClass.getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            Object value = null;

            try {
                value = field.get(validatingObject);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            Annotation[] annotations = field.getDeclaredAnnotations();
            for (Annotation annotation : annotations) {
                if (value != null) {
                    AnnotationProcessor processor = AnnotationProcessor.getRelevantProcessor(annotation);
                    ValidationError error = processor.processAnnotation(value, field);
                    errors.add(error);
                }
            }
        }
        return errors;
    }

    public static void print(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            System.out.println(error.toString());
        }
    }
}
