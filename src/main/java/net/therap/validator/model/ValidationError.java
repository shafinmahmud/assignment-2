package net.therap.validator.model;

/**
 * @author shafin
 * @since 11/7/16
 */
public class ValidationError {

    private String fieldName;
    private String typeName;
    private String errorMessage;

    public ValidationError(String fieldName, String typeName, String errorMessage) {
        this.fieldName = fieldName;
        this.typeName = typeName;
        this.errorMessage = errorMessage;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return fieldName + "(" + typeName + "): " + errorMessage;
    }
}
