package net.therap.validator.controller;

import net.therap.validator.service.AnnotatedValidator;
import net.therap.validator.model.Person;
import net.therap.validator.model.ValidationError;

import java.util.List;

/**
 * @author shafin
 * @since 11/7/16
 */
public class Main {

    public static void main(String[] args) {
        Person p = new Person("", 17);
        List<ValidationError> errors = AnnotatedValidator.validate(p);
        AnnotatedValidator.print(errors);
    }
}
